<%@ page import="java.util.List" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Beer Recommendations</title>
</head>
<body>
<h1>Beer Recommendations - JSP Version</h1>
<%
    List<String> beers = (List<String>) request.getAttribute("beers");
    for (String beer: beers) {
        out.println("<br>" + beer);
    }
%>
</body>
</html>